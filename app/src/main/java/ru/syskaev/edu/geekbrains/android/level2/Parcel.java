package ru.syskaev.edu.geekbrains.android.level2;

import java.io.Serializable;

public class Parcel implements Serializable {
    static private int index;
    static private String cityName;
    static private boolean firstTimeUse = true;

    public static boolean isFirstTimeUse() { return firstTimeUse; }
    public static void setIndex(int index) { Parcel.index = index; }
    public static void setCityName(String cityName) { Parcel.cityName = cityName; firstTimeUse = false; }
    static public int getIndex() {
        return index;
    }
    static public String getCityName() {
        return cityName;
    }


}
