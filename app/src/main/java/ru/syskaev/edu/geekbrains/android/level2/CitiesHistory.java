package ru.syskaev.edu.geekbrains.android.level2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CitiesHistory {

    private static final int MINIMAL_LIST_CAPACITY = 100;

    private static Map<Integer, List<Double>> historyMap = new HashMap<>();

    private CitiesHistory() {}

    public static void checkEmptyList(int cityIndex) {
        List<Double> list = historyMap.get(cityIndex);
        if(list == null) {
            list = new ArrayList<>(MINIMAL_LIST_CAPACITY);
            historyMap.put(cityIndex, list);
        }
    }

    public static void setValueByCityIndex(double temperatureValue, int cityIndex) {
        List<Double> list = historyMap.get(cityIndex);
        if(list == null) {
            list = new ArrayList<>(MINIMAL_LIST_CAPACITY);
            historyMap.put(cityIndex, list);
        }
        list.add(temperatureValue);
    }

    public static List<Double> getHistoryByCityIndex(int cityIndex) {
        return historyMap.get(cityIndex);
    }

}
