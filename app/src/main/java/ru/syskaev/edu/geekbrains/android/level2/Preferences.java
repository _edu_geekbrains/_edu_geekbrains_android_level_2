package ru.syskaev.edu.geekbrains.android.level2;

class Preferences {
    public static final String PREF_FILE_NAME = "weather";
    public static final String PREF_CITY_NAME_KEY = "city_name";
    public static final String PREF_CITY_NAME_DEFAULT = "Moscow";
    public static final String PREF_CITY_INDEX_KEY = "city_index";
    public static final int PREF_CITY_INDEX_DEFAULT = 0;
}

