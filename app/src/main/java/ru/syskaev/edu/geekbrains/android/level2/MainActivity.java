package ru.syskaev.edu.geekbrains.android.level2;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.Locale;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String LANG_TAG = "languageToLoad";
    private static final String FRAGMENT_TAG = "cities_fragment_tag";
    private static final String TEST_EMAIL = "anyadress@email.cc";
    public static final int PERMISSION_REQUEST_CODE = 111;

    enum ContentType {MAIN, SEARCH, SENSORS, GEOLOC, INFO}
    private ContentType prevContentType;
    private ContentType contentType = ContentType.MAIN;
    private String provider;

    private String languageToLoad = "en";
    private NavigationView navigationView;
    private SensorManager sensorManager;
    private TextView temperatureText = null;
    private TextView humidityText = null;
    private SensorEventListener temperatureListener = null;
    private SensorEventListener humidityListener = null;
    private ViewGroup customViewLayout = null;
    private CustomView customView = null;
    private boolean serviceCreated = false;
    private SharedPreferences preferences = null;
    private EditText searchEditText = null;
    TextView searchNotification = null;
    private Button searchButton = null;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private TextView geoText = null;
    private TextView coordText = null;
    private Button relocateButton = null;

    public void setContentType(ContentType contentType) {
        prevContentType = this.contentType;
        this.contentType = contentType;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLanguage();
        preferences = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
        loadPref();
        sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        if(!serviceCreated) startTemperatureService();
        setContentView(R.layout.activity_main);
        initUI();
    }

    private void initUI() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.empty, R.string.empty);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(contentType == ContentType.MAIN)
            setFragment();
        else if(contentType == ContentType.SEARCH) {
            searchButton.setEnabled(true);
            searchNotification.setText(getString(R.string.enter_city));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        removeFragment();
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        editPref();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.language) changeLanguage();
        else if (id == R.id.support) mailToSupport();
        else return super.onOptionsItemSelected(item);

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.nav_main:
                if(contentType != ContentType.MAIN) {
                    setContentType(ContentType.MAIN);
                    setContent(R.layout.content_cities);
                    navigationView.setCheckedItem(R.id.nav_main);
                }
                break;
            case R.id.nav_search:
                if(contentType != ContentType.SEARCH) {
                    setContentType(ContentType.SEARCH);
                    setContent(R.layout.content_search);
                    navigationView.setCheckedItem(R.id.nav_search);
                }
                break;
            case R.id.nav_sensors:
                if(contentType != ContentType.SENSORS) {
                    setContentType(ContentType.SENSORS);
                    setContent(R.layout.content_sensors);
                    navigationView.setCheckedItem(R.id.nav_sensors);
                }
                break;
            case R.id.nav_geoloc:
                if(contentType != ContentType.GEOLOC) {
                    setContentType(ContentType.GEOLOC);
                    setContent(R.layout.content_gps);
                    navigationView.setCheckedItem(R.id.nav_geoloc);
                }
                break;
            case R.id.nav_info:
                if(contentType != ContentType.INFO) {
                    setContentType(ContentType.INFO);
                    setContent(R.layout.content_info);
                    navigationView.setCheckedItem(R.id.nav_info);
                }
                break;
            case R.id.language:
                changeLanguage();
                break;
            case R.id.support:
                mailToSupport();
        }

        DrawerLayout drawer = findViewById (R.id.drawer_layout);
        drawer.closeDrawer (GravityCompat.START);
        return true;
    }

    private void setContent(int layoutId) {

        if(prevContentType == ContentType.SENSORS) {
            if(temperatureListener != null)
                sensorManager.unregisterListener(temperatureListener);
            if(humidityListener != null)
                sensorManager.unregisterListener(humidityListener);
            temperatureListener = humidityListener = null;
            customView.setExistFlag(false);
            customViewLayout.removeAllViewsInLayout();
        }

        ViewGroup parent = findViewById(R.id.content);
        parent.removeAllViews();
        View newContent = getLayoutInflater().inflate(layoutId, parent, false);
        parent.addView(newContent);

        if(prevContentType == ContentType.MAIN)
            removeFragment();
        else if(contentType == ContentType.MAIN)
            setFragment();

        if(contentType == ContentType.SENSORS) {
            prepareSensors();
            addCustomView();
        }

        if(contentType == ContentType.GEOLOC) {
            prepareGeoLoc();
        }

        if(contentType == ContentType.SEARCH)
            prepareSearch();

    }

    private void prepareSensors() {
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        if(sensor != null) {
            temperatureText = findViewById(R.id.current_temperature_value);
            temperatureListener = new CustomSensorListener(temperatureText);
            sensorManager.registerListener(temperatureListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        if(sensor != null) {
            humidityText = findViewById(R.id.current_humidity_value);
            humidityListener = new CustomSensorListener(humidityText);
            sensorManager.registerListener(humidityListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    private void addCustomView() {
        customViewLayout = findViewById(R.id.custom_view_layout);
        customView = new CustomView(this);
        customView.setLayoutParams(new FrameLayout.LayoutParams
                (FrameLayout.LayoutParams.MATCH_PARENT, 600));
        customViewLayout.addView(customView);
    }

    private void prepareSearch() {
        searchEditText = findViewById(R.id.edit_text);
        searchNotification = findViewById(R.id.notification);
        searchButton = findViewById(R.id.search_button);
        searchButton.setOnClickListener(v -> clickSearchButton());
    }

    private void clickSearchButton() {
        searchButton.setEnabled(false);
        searchNotification.setText(getText(R.string.wait_pls));
        TemperatureTaskAlt task = new TemperatureTaskAlt(searchEditText.getText().toString(), this);
        task.execute();
    }

    void onCityNotFound() {
        searchButton.setEnabled(true);
        searchNotification.setText(getString(R.string.city_not_found));
    }

    void startDetailActivity() {
        removeFragment();
        Parcel.setCityName(searchEditText.getText().toString());
        Parcel.setIndex(0);
        Intent intent = new Intent();
        intent.setClass(this, DetailActivity.class);
        startActivity(intent);
    }

    private void startDetailActivityForGeoLoc() {
        removeFragment();
        geoText.setText(R.string.current_coords);
        locationManager.removeUpdates(locationListener);
        relocateButton.setVisibility(View.VISIBLE);
        relocateButton.setEnabled(true);
        relocateButton.setOnClickListener(v -> {
            relocateButton.setEnabled(false);
            requestLocation()
            ;});
        Parcel.setCityName(coordText.getText().toString());
        Parcel.setIndex(0);
        Intent intent = new Intent();
        intent.setClass(this, DetailActivity.class);
        startActivity(intent);
    }

    private void setFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        if(fragment == null) fragment = new CitiesFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.cities_frame, fragment, FRAGMENT_TAG);
        ft.commit();
    }

    private void removeFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        if(fragment == null) return;
        if(fragment.isAdded()) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.remove(fragment);
            ft.commit();
        }
    }

    private void startTemperatureService() {
        Intent intent = new Intent(this, TemperatureService.class);
        startService(intent);
        serviceCreated = true;
    }

    private void setLanguage() {
        languageToLoad = getSharedPreferences(LANG_TAG, MODE_PRIVATE)
                .getString(LANG_TAG, "en");
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    private void changeLanguage() {
        if(languageToLoad.contains("en"))
            languageToLoad = "ru";
        else
            languageToLoad = "en";
        SharedPreferences languagepref = getSharedPreferences(LANG_TAG, MODE_PRIVATE);
        SharedPreferences.Editor editor = languagepref.edit();
        editor.putString(LANG_TAG, languageToLoad);
        editor.commit();
        refreshActivity();
    }

    private void refreshActivity() {
        Intent refresh = new Intent(this, this.getClass());
        refresh.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(refresh);
    }

    private void mailToSupport() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        intent.putExtra(Intent.EXTRA_EMAIL, TEST_EMAIL);
        startActivity(Intent.createChooser(intent, "Send Email"));
    }

    private void loadPref() {
        Parcel.setCityName(preferences.getString(
                Preferences.PREF_CITY_NAME_KEY,
                Preferences.PREF_CITY_NAME_DEFAULT));
        Parcel.setIndex(preferences.getInt(
                Preferences.PREF_CITY_INDEX_KEY,
                Preferences.PREF_CITY_INDEX_DEFAULT));
    }

    private void editPref() {
        preferences.edit()
                .putString(Preferences.PREF_CITY_NAME_KEY, Parcel.getCityName())
                .putInt(Preferences.PREF_CITY_INDEX_KEY, Parcel.getIndex())
                .apply();
    }

    class CustomSensorListener implements SensorEventListener {

        TextView textView;

        CustomSensorListener(TextView textView) {this.textView = textView; }

        @Override
        public void onAccuracyChanged (Sensor sensor, int accuracy ){ }

        @Override
        public void onSensorChanged (SensorEvent event ){
            textView.setText(Float.toString(event.values[0]));
        }
    }

    private void prepareGeoLoc() {
        geoText = findViewById(R.id.geo_text);
        coordText = findViewById(R.id.coords);
        relocateButton = findViewById(R.id.relocate_button);
        relocateButton.setVisibility(View.INVISIBLE);
        relocateButton.setEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            requestLocation();
        } else {
            requestLocationPermissions();
        }
    }

    private void requestLocation() {
        geoText.setText(R.string.wait_pls);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            return;
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        provider = locationManager.getBestProvider(criteria, true);
        if (provider != null) {
            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    double latitude = location.getLatitude();  // Широта
                    double longitude = location.getLongitude(); // Долгота
                    coordText.setText(String.format("%.3f / %.3f", latitude, longitude));
                    startDetailActivityForGeoLoc();
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) { }

                @Override
                public void onProviderEnabled(String provider) { }

                @Override
                public void onProviderDisabled(String provider) { }

            };
            locationManager.requestLocationUpdates(provider, 10000, 10, locationListener);
        }
    }

    private void requestLocationPermissions() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    },
                    PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length == 2 &&
                    (grantResults[0] == PackageManager.PERMISSION_GRANTED || grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                requestLocation();
            }
        }
    }

}
