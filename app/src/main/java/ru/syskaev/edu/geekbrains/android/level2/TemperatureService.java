package ru.syskaev.edu.geekbrains.android.level2;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;


public class TemperatureService extends Service {

    public static final int MSG_HELLO = 1;
    public static final int MSG_REQUEST_DATA = 2;
    public static final int MSG_RETURN_DATA = 3;

    private final Messenger localMessenger = new Messenger(new IncomingHandler());
    private Messenger outerMessenger = null;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return localMessenger.getBinder();
    }

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_HELLO:
                    outerMessenger = (Messenger) msg.obj;
                    Message hiMsg = Message.obtain(null, MSG_HELLO);
                    try {
                        outerMessenger.send(hiMsg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    break;
                case MSG_REQUEST_DATA:
                    String cityName = (String) msg.obj;
                    if(Character.isAlphabetic(cityName.charAt(0))) {
                        TemperatureTask task =
                                new TemperatureTask(outerMessenger, (String) msg.obj, false);
                        task.execute();
                    }
                    else {
                        TemperatureTask task =
                                new TemperatureTask(outerMessenger, (String) msg.obj, true);
                        task.execute();
                    }
                default:
                    super.handleMessage(msg);
            }
        }
    }

}
