package ru.syskaev.edu.geekbrains.android.level2;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;


public class DetailFragment extends Fragment {

    private static final String CITY_INDEX_KEY = "city_index";
    private static final String ICON_URL = "http://openweathermap.org/img/w/";
    private static final double HPA_TO_MMGH = 0.750062;

    private TextView detailText1, detailText2, detailText3, detailText4, detailText5;
    private final ServiceConnection connection = connection();
    private final Messenger localMessenger = new Messenger(new IncomingHandler());
    private Messenger serviceMessenger = null;
    private ImageView weatherIcon;

    public static DetailFragment create() {
        DetailFragment f = new DetailFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_detail, container, false);

        prepareHistoryButton(layout);
        CitiesHistory.checkEmptyList(Parcel.getIndex());

        weatherIcon = layout.findViewById(R.id.weatherIcon);
        weatherIcon.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim);
            v.startAnimation(animation);
        });

        TextView cityName = layout.findViewById(R.id.cityName);

        detailText1 = layout.findViewById(R.id.detail1Value);
        detailText2 = layout.findViewById(R.id.detail2Value);
        detailText3 = layout.findViewById(R.id.detail3Value);
        detailText4 = layout.findViewById(R.id.detail4Value);
        detailText5 = layout.findViewById(R.id.detail5Value);

        if (getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE)
            weatherIcon.setVisibility(View.INVISIBLE);

        cityName.setText(Parcel.getCityName());
        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();
        prepareServiceBind();
    }

    private void handleResponse(OpenWeatherResponse owResponse) {
        detailText1.setText(String.format("%.1f C", owResponse.main.temp));
        CitiesHistory.setValueByCityIndex(owResponse.main.temp, Parcel.getIndex());
        detailText2.setText(String.format
                ("%.0f " + getResources().getString(R.string.mmhg), owResponse.main.pressure * HPA_TO_MMGH));
        detailText3.setText(String.format("%.0f %%", owResponse.main.humidity));
        detailText4.setText(String.format
                ("%.1f " + getResources().getString(R.string.mps), owResponse.wind.speed));
        detailText5.setText(getWindDegreeDescription(owResponse.wind.deg));
        Picasso.with(getContext())
                .load(ICON_URL + owResponse.weather[0].icon + ".png").into(weatherIcon);
    }

    private String getWindDegreeDescription(double degD) {
        int degI = (int)(degD + 22.5);
        return getResources().getStringArray(R.array.direction)[degI / 45];
    }

    @Override
    public void onStop() {
        try {
            getActivity().unbindService(connection);
        }
        catch (IllegalArgumentException e) {
            Log.v("WTF", "Double stop?");
        }
        super.onStop();
    }

    private void prepareServiceBind() {
        getActivity().bindService(new Intent(getContext(), TemperatureService.class), connection,
                Context.BIND_AUTO_CREATE);

    }

    private ServiceConnection connection() {
        return new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                serviceMessenger = new Messenger(service);
                Message msg = Message.obtain(null, TemperatureService.MSG_HELLO, localMessenger);
                try {
                    serviceMessenger.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onServiceDisconnected(ComponentName name) {
                serviceMessenger = null;
            }
        };
    }

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case TemperatureService.MSG_HELLO:
                    Message reqMsg = Message
                            .obtain(null, TemperatureService.MSG_REQUEST_DATA, Parcel.getCityName());
                    try {
                        serviceMessenger.send(reqMsg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    break;
                case TemperatureService.MSG_RETURN_DATA:
                    handleResponse((OpenWeatherResponse) msg.obj);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private void prepareHistoryButton(View layout) {
        Button historyButton = layout.findViewById(R.id.buttonHistory);
        historyButton.setOnClickListener((view) -> {
            Intent history = new Intent(view.getContext(), HistoryActivity.class);
            history.putExtra(CITY_INDEX_KEY, Parcel.getIndex());
            startActivity(history);
        });
    }

}
