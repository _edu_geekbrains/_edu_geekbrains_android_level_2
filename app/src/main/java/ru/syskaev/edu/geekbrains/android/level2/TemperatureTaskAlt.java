package ru.syskaev.edu.geekbrains.android.level2;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;


public class TemperatureTaskAlt extends AsyncTask<String, String , String> {

    private static final String APPID_KEY = "APPID";
    private static final String APPID_VALUE = "3f5c63f6091aa5c630bf622fadc208e5";
    private static final String UNITS_KEY = "units";
    private static final String UNITS_VALUE = "metric";
    private static final String CITY_KEY = "q";
    private static final int SUCCESS_CODE = 200;

    private OkHttpClient client;
    private String cityName;
    private boolean errorFlag;
    private OpenWeatherResponse response;
    private Context context;

    public TemperatureTaskAlt(String cityName, Context context) {
        super();
        errorFlag = false;
        this.cityName = cityName;
        this.context = context;
    }

    @Override
    protected String doInBackground(String... strings) {
        client = new OkHttpClient().newBuilder().dns(new EasyDns()).build();
        connectByOkHttp();
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(errorFlag || response.cod != SUCCESS_CODE)
            ((MainActivity)context).onCityNotFound();
        else
            ((MainActivity)context).startDetailActivity();
    }

    private OpenWeatherResponse getResponse(String jsonString) {
        Gson gson = (new GsonBuilder()).create();
        return gson.fromJson(jsonString, OpenWeatherResponse.class);
    }

    private void connectByOkHttp() {
        try {
            HttpUrl url = HttpUrl.get("https://api.openweathermap.org/data/2.5/weather");
            url = url.newBuilder()
                    .addQueryParameter(CITY_KEY, cityName)
                    .addQueryParameter(APPID_KEY, APPID_VALUE)
                    .addQueryParameter(UNITS_KEY, UNITS_VALUE)
                    .build();
            Log.v("OkHttp", url.toString());
            Request request = new Request.Builder().url(url).build();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();
            if (responseBody != null)
                this.response = getResponse(responseBody.string());
            else
                errorFlag = true;
        } catch(IOException e){
            errorFlag = true;
            e.printStackTrace();
        }
    }

}
