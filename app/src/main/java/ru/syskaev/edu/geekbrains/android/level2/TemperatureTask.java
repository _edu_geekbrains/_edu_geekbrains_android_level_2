package ru.syskaev.edu.geekbrains.android.level2;

import android.os.AsyncTask;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;


public class TemperatureTask extends AsyncTask<String, String , String> {

    private static final String APPID_KEY = "APPID";
    private static final String APPID_VALUE = "3f5c63f6091aa5c630bf622fadc208e5";
    private static final String UNITS_KEY = "units";
    private static final String UNITS_VALUE = "metric";
    private static final String CITY_KEY = "q";
    private static final String LATITUDE_KEY = "lat";
    private static final String LONGITUDE_KEY = "lon";
    private static final int SUCCESS_CODE = 200;

    private Messenger outerMessenger;
    private OkHttpClient client;
    private String cityName;
    private String[] coords;
    private boolean errorFlag;
    private OpenWeatherResponse response;
    private boolean isCoord;

    public TemperatureTask(Messenger messenger, String cityName, boolean isCoord) {
        super();
        errorFlag = false;
        outerMessenger = messenger;
        this.cityName = cityName;
        this.isCoord = isCoord;
        if(isCoord)
            coords = cityName.split(" / ");
    }

    @Override
    protected String doInBackground(String... strings) {
        client = new OkHttpClient().newBuilder().dns(new EasyDns()).build();
        connectByOkHttp();
        if(!errorFlag && response.cod == SUCCESS_CODE)
            doDBTask();
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(errorFlag || response.cod != SUCCESS_CODE)
            return;
        Message ansMsg = Message.obtain(null, TemperatureService.MSG_RETURN_DATA, response);
        try {
            outerMessenger.send(ansMsg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private OpenWeatherResponse getOWResponse(String jsonString) {
        Gson gson = (new GsonBuilder()).create();
        return gson.fromJson(jsonString, OpenWeatherResponse.class);
    }

    private void connectByOkHttp() {
        try {
            HttpUrl url = HttpUrl.get("https://api.openweathermap.org/data/2.5/weather");
            if(isCoord)
                url = url.newBuilder()
                        .addQueryParameter(LATITUDE_KEY, coords[0])
                        .addQueryParameter(LONGITUDE_KEY, coords[1])
                        .addQueryParameter(APPID_KEY, APPID_VALUE)
                        .addQueryParameter(UNITS_KEY, UNITS_VALUE)
                        .build();
            else
                url = url.newBuilder()
                        .addQueryParameter(CITY_KEY, cityName)
                        .addQueryParameter(APPID_KEY, APPID_VALUE)
                        .addQueryParameter(UNITS_KEY, UNITS_VALUE)
                        .build();
            Log.v("OkHttp", url.toString());
            Request request = new Request.Builder().url(url).build();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();
            if (responseBody != null)
                this.response = getOWResponse(responseBody.string());
            else
                errorFlag = true;
        } catch(IOException e){
            errorFlag = true;
            e.printStackTrace();
        }
    }

    private void doDBTask() {
        DBHelper dbHelper = new DBHelper();
        dbHelper.upgrade(response);
        dbHelper.close();
    }

}
