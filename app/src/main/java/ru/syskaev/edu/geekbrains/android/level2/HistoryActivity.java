package ru.syskaev.edu.geekbrains.android.level2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;


public class HistoryActivity extends AppCompatActivity {

    private static final String CITY_INDEX_KEY = "city_index";

    private int cityIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        cityIndex = getIntent().getIntExtra(CITY_INDEX_KEY, -1);
        initList();
    }

    private void initList() {
        List<Double> history = CitiesHistory.getHistoryByCityIndex(cityIndex);
        RecyclerView recyclerView = findViewById(R.id.history_container);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        HistoryAdapter historyAdapter = new HistoryAdapter(history);
        recyclerView.setAdapter(historyAdapter);
    }

}
