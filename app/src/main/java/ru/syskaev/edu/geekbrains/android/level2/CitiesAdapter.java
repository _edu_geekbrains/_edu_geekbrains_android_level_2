package ru.syskaev.edu.geekbrains.android.level2;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {

    private final List<String> cities;
    private final Resources res;
    private CitiesFragment parentFragment;
    private int currentCityIndex;

    public CitiesAdapter(List<String> list, CitiesFragment parentFragment) {
        this.cities = list;
        this.parentFragment = parentFragment;
        res = parentFragment.getResources();
        currentCityIndex = Parcel.getIndex();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        CardView view = (CardView) layoutInflater.inflate(R.layout.item_city, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.textView.setText(cities.get(i));
        if(i == currentCityIndex && res.getConfiguration().orientation == ORIENTATION_LANDSCAPE)
            viewHolder.itemView.setCardBackgroundColor(res.getColor(R.color.colorAccent));
        else
            viewHolder.itemView.setCardBackgroundColor(res.getColor(R.color.colorSkyBlue));
        TypedArray imgs = res.obtainTypedArray(R.array.coatofarms_imgs);
        viewHolder.imageView.setImageResource(imgs.getResourceId(i, -1));
        viewHolder.itemView.setOnClickListener(v -> parentFragment.onItemClick(viewHolder.textView, i));
    }

    @Override
    public int getItemCount() { return cities.size();}

    public void setCurrentCityIndex(int currentCityIndex) {
        int oldIndex = this.currentCityIndex;
        this.currentCityIndex = currentCityIndex;
        notifyItemChanged(oldIndex);
        notifyItemChanged(currentCityIndex);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        ImageView imageView;
        CardView itemView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.textView = itemView.findViewById(R.id.text);
            this.imageView = itemView.findViewById(R.id.image);
            this.itemView = (CardView)itemView;
        }
    }

}
