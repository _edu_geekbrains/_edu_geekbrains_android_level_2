package ru.syskaev.edu.geekbrains.android.level2;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "cities.db";

    private static final String TABLE_NAME = "cities";

    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_CITY = "city";
    private static final String COLUMN_TEMP = "temperature";
    private static final String COLUMN_PRES = "pres";
    private static final String COLUMN_HUM = "hum";
    private static final String COLUMN_WIND_S = "wind_s";
    private static final String COLUMN_WIND_D = "wind_d";

    public DBHelper() {
        super(App.getContext(), DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.v("TAG", "onCreate: " + Thread.currentThread().toString());
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_CITY + " TEXT," +
                COLUMN_TEMP + " DOUBLE," +
                COLUMN_PRES + " DOUBLE," +
                COLUMN_HUM + " DOUBLE," +
                COLUMN_WIND_S + " DOUBLE," +
                COLUMN_WIND_D + " DOUBLE)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,  int newVersion) { }

    public void upgrade(OpenWeatherResponse response) {
        SQLiteDatabase database = getWritableDatabase();
        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + TABLE_NAME +
                        " WHERE " + COLUMN_CITY + " = \"" + response.name + "\"",
                null);
        if(cursor.getCount() == 0)
            database.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                    COLUMN_CITY + "," + COLUMN_TEMP + "," + COLUMN_PRES + "," +
                    COLUMN_HUM + "," + COLUMN_WIND_S + "," + COLUMN_WIND_D + ") VALUES (\"" +
                    response.name + "\"," + response.main.temp + "," +
                    response.main.pressure + "," + response.main.humidity + "," +
                    response.wind.speed + "," + response.wind.deg + ")");
        else {
            cursor.moveToFirst();
            int tempId = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
            database.execSQL("UPDATE " + TABLE_NAME + " SET " +
                    COLUMN_TEMP + " = " + response.main.temp + ", " +
                    COLUMN_PRES + " = " + response.main.pressure + ", " +
                    COLUMN_HUM + " = " + response.main.humidity + ", " +
                    COLUMN_WIND_S + " = " + response.wind.speed + ", " +
                    COLUMN_WIND_D + " = " + response.wind.deg +
                    " WHERE " + COLUMN_ID + " = " + tempId);
        }
    }

}
