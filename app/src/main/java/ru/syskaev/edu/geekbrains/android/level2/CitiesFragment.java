package ru.syskaev.edu.geekbrains.android.level2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

public class CitiesFragment extends Fragment {

    boolean isExistDetail;
    RecyclerView recyclerView;
    CitiesAdapter citiesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = view.findViewById(R.id.cities_container);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        List<String> cities = Arrays.asList(getResources().getStringArray(R.array.cities));

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        citiesAdapter = new CitiesAdapter(cities, this);
        recyclerView.setAdapter(citiesAdapter);

        View detailsFrame = getActivity().findViewById(R.id.detail_fragment);
        isExistDetail = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

        if (isExistDetail){
            showDetail();
        }
    }

    public void onItemClick(View v, int position) {
        TextView cityNameView = (TextView) v;
        Parcel.setIndex(position);
        Parcel.setCityName(cityNameView.getText().toString());
        citiesAdapter.setCurrentCityIndex(position);
        recyclerView.refreshDrawableState();
        showDetail();
    }

    @Override
    public void onStart() {
        super.onStart();
        recyclerView.scrollToPosition(Parcel.getIndex());
    }

    private void showDetail() {
        if (isExistDetail) {
            DetailFragment detail = DetailFragment.create();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.detail_fragment, detail);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();
        }
        else {
            Intent intent = new Intent();
            intent.setClass(getActivity(), DetailActivity.class);
            startActivity(intent);
        }
    }

}
