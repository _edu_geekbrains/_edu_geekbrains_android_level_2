package ru.syskaev.edu.geekbrains.android.level2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.view.View;

public class CustomView extends View {

    private static final int MAX_D = 120;
    private static final int MIN_R = 120;
    private static final int MAX_B = 225, MIN_B = 25;


    private int dx = MAX_D;
    private int dy = 0;
    private int ddx = -3;
    private int ddy = 3;

    private int A = 255, R = 200, G = 40, B = MIN_B;
    private int db = 2;

    Paint paint;
    boolean existFlag;
    Point center;

    public CustomView(Context context) {
        super(context);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        existFlag = true;
        (new DrawThread(this)).start();
    }

    public boolean isExist() { return existFlag; }
    public void setExistFlag(boolean existFlag) { this.existFlag = existFlag; }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        center = new Point(getWidth() / 2, getHeight() / 2);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        paint.setColor(Color.argb(A, R, G, B));
        RectF rect = new RectF(center.x - MIN_R - dx, center.y + MIN_R + dy,
                center.x + MIN_R + dx,  center.y - MIN_R - dy);
        canvas.drawOval(rect, paint);
        dx += ddx;
        if(dx >= MAX_D || dx <= 0) ddx *= -1;
        dy += ddy;
        if(dy >= MAX_D || dy <= 0) ddy *= -1;
        B += db;
        if(B >= MAX_B || B <= MIN_B) db *= -1;
    }

    class DrawThread extends Thread {

        private static final int DRAW_THREAD_SLEEP_TIME = 50;

        private CustomView view;

        public DrawThread(CustomView view) {
            this.view = view;
        }

        @Override
        public void run() {
            while(view.isExist()) {
                try {
                    Thread.sleep(DRAW_THREAD_SLEEP_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                view.invalidate();
            }
        }
    }

}
