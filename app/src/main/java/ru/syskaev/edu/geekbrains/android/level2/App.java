package ru.syskaev.edu.geekbrains.android.level2;

import android.app.Application;

public class App extends Application {

    private static App instance = null;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static App getContext() {
        return instance;
    }
}
