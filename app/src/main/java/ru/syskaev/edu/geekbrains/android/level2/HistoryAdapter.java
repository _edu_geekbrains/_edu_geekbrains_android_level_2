package ru.syskaev.edu.geekbrains.android.level2;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private final List<Double> history;

    public HistoryAdapter(List<Double> list) {
        this.history = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        TextView view = (TextView) layoutInflater.inflate(R.layout.item_history, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        String tempString = String.format("2018.11.%d:   %.1f C", i, history.get(i));
        viewHolder.textView.setText(tempString);
        viewHolder.itemView.setBackgroundColor(getColorAsIntByTemperature((int) Math.round(history.get(i))));
    }

    private int getColorAsIntByTemperature(Integer temperature) {
        int R = 150 + temperature * 2;
        int G = 80;
        int B = 125 - temperature * 2;
        return Color.rgb(R, G, B);
    }

    @Override
    public int getItemCount() {
        return history.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        View itemView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.textView = itemView.findViewById(R.id.textView);
            this.itemView = itemView;
        }
    }

}
