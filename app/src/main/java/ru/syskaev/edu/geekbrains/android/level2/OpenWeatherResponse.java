package ru.syskaev.edu.geekbrains.android.level2;

public class OpenWeatherResponse {

    long id;
    String name;
    int cod;
    Main main;
    Wind wind;
    Weather[] weather;

    public class Weather {
        String icon;
    }

    public class Main {
        double temp;
        double pressure;
        double humidity;
    }

    public class Wind {
        double speed;
        double deg;
    }

}
