package ru.syskaev.edu.geekbrains.android.level2;

import org.xbill.DNS.Address;
import org.xbill.DNS.ExtendedResolver;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.SimpleResolver;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;

import okhttp3.Dns;


public class EasyDns implements Dns {

    private boolean mInitialized;

    @Override
    public List<InetAddress> lookup(String hostname) throws UnknownHostException {
        init();
        return Collections.singletonList(Address.getByName(hostname));
    }

    private void init() {
        if (mInitialized) return;
        else mInitialized = true;

        try {
            Resolver yandexResolver = new SimpleResolver("77.88.8.8");
            //Resolver googleFirstResolver = new SimpleResolver("8.8.8.8");
            //Resolver googleSecondResolver = new SimpleResolver("8.8.4.4");
            //Resolver amazonResolver = new SimpleResolver("205.251.198.30");
            Lookup.setDefaultResolver(new ExtendedResolver(new Resolver[]{yandexResolver}));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

}
